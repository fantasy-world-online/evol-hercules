// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_ENUM_ESITYPE
#define EVOL_MAP_ENUM_ESITYPE

enum esi_type
{
    SI_PHYSICAL_SHIELD = 966,
    //SI_TMW2_INCSTR = 970,
    SI_TMW2_INCAGI = 971,
    SI_TMW2_INCVIT = 972,
    SI_TMW2_INCINT = 973,
    SI_TMW2_INCDEX = 974,
    SI_TMW2_INCLUK = 975,
    SI_TMW2_INCHIT = 976,
    SI_TMW2_INCFLEE = 977,
    SI_TMW2_WALKSPEED = 978,
    SI_TMW2_INCMHPRATE = 979,
    SI_TMW2_INCMSPRATE = 980,
};

#endif  // EVOL_MAP_ENUM_ESITYPE
