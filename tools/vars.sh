#!/usr/bin/env bash

# MAX_SKILL_DB 1510  + 62 = 1572
# MAX_SKILL_ID 10015 + 62 + 9963 = 20062
# SC_MAX       653   + 5  = 658
# SI_MAX       966   + 5  = 971
# MAX_EVOL_SKILLS           42
# EVOL_FIRST_SKILL          20000
# OLD_MAX_SKILL_DB          1510
# MAX_SKILL_TREE            110

# can be used for custom skill id: 10016 - 10036+20

export VARS=" -DOLD_MAX_SKILL_DB=1510 -DMAX_STORAGE=500 -DMAX_SKILL_DB=1572 -DMAX_SKILL_ID=20062 -DMAX_EVOL_SKILLS=62 -DEVOL_FIRST_SKILL=20000 -DMAX_SKILL_TREE=110 -DSC_MAX=658 -DSI_MAX=991 -DMAX_LEVEL=999 -DMIN_PACKET_DB=0x63 -DMAX_PACKET_DB=0x7531"
export CPPFLAGS="${VARS}"
